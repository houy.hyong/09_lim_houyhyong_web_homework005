import React from 'react'

export default function RightSidebar() {
  return (
    <div style={{ backgroundImage: "linear-gradient(rgba(0,0,0,0.2), rgba(0,0,0,0.6)), url(./images/bg.jpg)" }} className=' bg-scroll bg-center bg-cover  '>
      <div className='mr-8 ml-8'>
        

        <div className='flex gap-5 justify-end content-center items-center'>
          <img src='./images/notification.png' className="w-5 h-5 my-8" />
          <img src='./images/comment.png' className="w-5 h-5 my-8" />
          <img src='./images/lachlan.png' className="my-8 w-8 h-8 aspect-square rounded-full " />
        </div>
        

        <div className='flex justify-end'>
          <button className="bg-[#E3C565] rounded-lg hover:bg-[#D8B863] text-[#4B371C] py-2 px-4 ">
            My amazing trip
          </button>
        </div>


        <h1 className='font-bold text-3xl mb-5 text-slate-50 mt-96 backdrop-blur-lg backdrop-brightness-200'>I like laying down on the sand and looking at the moon.</h1>
        <p className='text-xl font-bold mb-3.5 text-slate-50 backdrop-blur-lg backdrop-brightness-200'>27 people going to this trip</p>


        <div className='flex content-center items-center justify-between '>
          <img src='./images/lachlan.png' className=" w-12 h-12 aspect-square rounded-full border-2 border-sky-700" />
          <img src='./images/raamin.png' className=" w-12 h-12 aspect-square rounded-full border-2 border-pink-700" />
          <img src='./images/nonamesontheway.png' className="w-12 h-12 aspect-square rounded-full border-2 border-orange-600" />
          <img src='./images/christina.png' className=" w-12 h-12 aspect-square rounded-full border-2 border-white" />
          <div className='w-12 h-12 aspect-square rounded-full bg-[#E3C565] border-2 border-yellow-700'>
            <p className='h-10 grid justify-center items-center '>23+</p>
          </div>
        </div>

      </div>
    </div>
  )
}
