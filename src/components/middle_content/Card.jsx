import React, { useState } from "react";

const Card = ({ data }) => {
  // console.log({ data });

  const [newData, setNewData] = useState([]);

  const pendingDoneButton = (item) => {
    // alert(item.status)
    if (item.status === "beach") {
      item.status = "forest";
    }
    else if (item.status === "forest") {
      item.status = "mountain"
    }
    else if (item.status === "mountain") {
      item.status = "beach"
    }
    else {
      item.status = "null"
    }
    setNewData([])
  }


  return (
    <div>
      <div className="flex justify-start gap-5 flex-wrap">
        {data.map((item) => (
          
          <div
            key={item.id}
            className="shadow-card flex flex-col rounded-xl bg-clip-border bg-[#004B57] text-white w-[360px] "
          >

            <div className="text-secondary flex-1 p-6 text-white">
              
              <h3 className="uppercase text-2xl">{item.title ? item.title : "null"}</h3>
              <p className="mb-3 line-clamp-3 my-5 text-justify">{item.description ? item.description : "null"}</p>
              <p className="">People Going</p>
              <p className="mb-3 text-2xl font-bold">{item.peopleGoing ? item.peopleGoing : "null"}</p>

              <div className="flex justify-between">
                {/* Button activity */}
                <div className="flex flex-col justify-center">
                  <button
                    className={`${item.status == "beach"
                      ? `btn text-white bg-[#f6af3b] hover:bg-[#f69000] font-medium rounded-lg text-sm mb-0 inline-flex justify-center items-center content-center w-36`
                      : item.status == "mountain"
                        ? `btn text-white bg-[#e75710] hover:bg-[#a43627] font-medium rounded-lg text-sm mb-0 inline-flex justify-center items-center content-center w-36`
                        : item.status == "forest"
                          ? `btn text-white bg-[#498964] hover:bg-[#239148] font-medium rounded-lg text-sm mb-0 inline-flex justify-center items-center content-center w-36`
                          : `btn text-white bg-gray-500 hover:bg-gray-500 font-medium rounded-lg text-sm mb-0 inline-flex justify-center items-center content-center w-36`

                      }`}
                    onClick={() => pendingDoneButton(item)}
                  >
                    {item.status ? item.status : "null"}
                  </button>
                </div>

                {/* Button read detail */}
                <div className="flex flex-col w-6/12 ">
                  <label
                    htmlFor="my-modal-3"
                    className="btn py-3 px-5 "
                    onClick={() => setNewData(item)}
                  >
                    Read Details
                  </label>

                  {/* {console.log(item.id)} */}


                  {/* Read detail output card */}
                  <div>
                    <input type="checkbox" id="my-modal-3" className="modal-toggle" />
                    <div className="modal backdrop-blur-[1px]">
                      <div className="modal-box relative text-gray-800 ">
                        <label
                          htmlFor="my-modal-3"
                          className="btn btn-sm btn-circle absolute right-2 top-2"
                        >
                          ✕
                        </label>
                        <div>
                          <h3 className=" text-[#a43627] uppercase text-2xl font-bold">{newData.title ? newData.title : "null"}</h3>
                          <p className="mb-3 text-justify py-4">{newData.description ? newData.description : "null"}</p>
                          <span>Around</span>
                          <span className="text-[#e75710] mb-3 text-2xl font-bold"> {newData.peopleGoing ? newData.peopleGoing : "null"} </span>
                          <span>people going there</span>
                        </div>

                      </div>
                    </div>
                  </div>

                </div>
              </div>
            </div>
          </div>
        ))}
      </div>
    </div>
  );
};

export default Card;
