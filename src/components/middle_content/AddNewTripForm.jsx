import React, { useState } from "react";

export default function AddNewTripForm({ data, setData }) {

  const [newData, setNewData] = useState([]);

  const handleInput = (e) => {
    setNewData({
      ...newData,
      [e.target.name]: e.target.value,

    })
    console.log(newData)
  }

  const submitHandler = (e) => {
    e.preventDefault();
    setData([
      ...data,
      {
        id: data.length + 1,
        ...newData
      }
    ])
    e.target.reset();


  }

  return (
    // form add new trip
    <div>
      <input type="checkbox" id="modal-3" className="modal-toggle" />
      <div className="modal">
        <div className="modal-box relative">
          <label
            htmlFor="modal-3"
            className="btn btn-sm btn-circle absolute right-2 top-2"
          >
            ✕
          </label>
          
          <div>
            <form onSubmit={submitHandler} >
              <div className="mb-6">
                <label className="block text-base font-semibold text-[#003738] mt-2.5">
                  Title
                </label>
                <input
                  type="text"
                  className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 "
                  name="title"
                  onChange={handleInput}
                  placeholder="Sihaknou Ville"
                // required
                />
              </div>

              <div className="mb-6">
                <label
                  className="block text-base font-semibold text-[#003738]"
                >
                  Description
                </label>
                <input
                  type="text"
                  className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5"
                  name="description"
                  placeholder="Happy place with beautiful beach"
                  onChange={handleInput}
                // required
                />
              </div>
              <div className="mb-6">
                <label
                  className="block text-base font-semibold text-[#003738]"

                >
                  People going
                </label>
                <input
                  type="number" min="0"
                  className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5"
                  name="peopleGoing"
                  placeholder="3200"
                  onChange={handleInput}
                // required
                />
              </div>
              <div className="mb-6">
                <label
                  className="block text-base font-semibold text-[#003738]"
                >
                  Type of adventure
                </label>
                <select
                  className="bg-gray-50 border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500"
                  name="status"
                  onChange={handleInput}
                >
                  <option value="null" selected disabled>---- Choose any option ----</option>
                  <option value="forest">Forest</option>
                  <option value="mountain">Mountain</option>
                  <option value="beach">Beach</option>
                </select>
              </div>
              <button

                className="text-white bg-sky-900 hover:bg-sky-600 focus:ring-4 focus:outline-none focus:ring-blue-300 font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 text-center"
              >
                <label htmlFor="modal-3">Submit</label>

              </button>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
}
