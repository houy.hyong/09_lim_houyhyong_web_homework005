import React from 'react'
import AddNewTripForm from './AddNewTripForm'
import Card from './Card'

export default function Content({ data, setData }) {
  // console.log(data)

  return (
    <div className='w-full'>

      <div className='mt-20 mb-8 flex justify-between'>
        {/* Title */}
        <span className='font-bold text-4xl flex justify-start'>Good Evening Team!</span>
        {/* Button add new trip */}
        <div>
          <label htmlFor="modal-3" className="btn ">add new trip</label>
          <AddNewTripForm data={data} setData={setData} />
        </div>
      </div>

      {/* output card */}
      <Card data={data} />

    </div>
  )
}
