import React from 'react'

export default function LeftSidebar() {
    return (
        <div className='flex justify-center bg-[#61B0B7]'>
            <div className='w-10'>
                
                <img src='./images/category_icon.png' className="w-6 h-6 my-20" />

                <div className="my-20">
                    <img src='./images/cube.png' className="w-6 h-6 my-7" />
                    <img src='./images/listreddot.png' className="w-6 h-6 my-7" />
                    <img src='./images/messengerreddot.png' className="w-6 h-6 my-7" />
                    <img src='./images/list.png' className=" w-6 h-6 my-7" />
                </div>

                <div className="my-20 ">
                    <img src='./images/success.png' className="w-6 h-6 my-7" />
                    <img src='./images/security.png' className="w-6 h-6 my-7" />
                    <img src='./images/users.png' className="w-6 h-6 my-7" />
                </div>

                <div className="my-20 w-7">
                    <img src='./images/lachlan.png' className="my-7 aspect-square rounded-full " />
                    <img src='./images/raamin.png' className="my-7 aspect-square rounded-full" />
                    <img src='./images/nonamesontheway.png' className="my-7 aspect-square rounded-full" />
                    <img src='./images/plus.png' className="my-7 rounded-full" />
                </div>

            </div>
        </div>
    )
}
