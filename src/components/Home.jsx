import React from 'react'
import LeftSidebar from './LeftSidebar'
import AddNewTripForm from './middle_content/AddNewTripForm'
import Content from './middle_content/Content'
import ReadDetails from './middle_content/ReadDetails'
import RightSidebar from './RightSidebar'

function Home({data, setData}) {

    // console.log(data);

    return (
        <div>
            <div className='flex justify-start'>
                <div className='fixed h-full overflow-y-auto w-32 flex grid '>
                    <LeftSidebar />
                </div>
            </div>


            <div className='flex justify-end'>
                <div className='fixed h-full overflow-y-auto bg-[#F8FAFC] flex w-96 '>
                    <RightSidebar />
                </div>
            </div>

            <div className='flex ml-32 mr-96 px-12'><Content data={data} setData={setData}/></div>

        </div>
    )
}

export default Home
